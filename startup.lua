local os = require("os")
local io = require("io")

package.path = debug.getinfo(1).source:match("@?(.*/)") .. "/lua/?.lua"

local c = require("config")

for _, service in pairs(c.services) do
    io.write(service .. ": ")

    local pidof_cmd = "pidof " .. service

    local pidof_stdout = io.popen(pidof_cmd, "r")

    if pidof_stdout == nil then
        error("got nil stdout from `" .. pidof_cmd .. "`")
    end

    local linect = 0
    for line in pidof_stdout:lines() do
        -- service is running
        if string.len(line) > 0 then
            linect = linect + 1
            print(line)
        end
    end

    -- service is not running
    if linect == 0 then

        local logname = c.service_bins[service] or service
        local logpath = c.logdir .. "/" .. logname

        local cmd = service .. " > " .. logpath .. " 2>&1 & disown"
        print(cmd)
        os.execute(cmd)
    end
end
