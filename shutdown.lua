local os = require("os")

package.path = package.path .. ";" .. os.getenv("PWD") .. "/lua/?.lua"

local c = require("config")

print()

for _, service in pairs(c.services) do
    local cmd = "killall " .. (c.service_bins[service] or service)
    print("cmd: " .. cmd)
    os.execute(cmd)
    print()
end

