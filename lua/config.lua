local M = {
    logdir = os.getenv("HOME") .. "/.mylocal/log",
    services = {
        -- auth fallback
        "/usr/lib/polkit-kde-authentication-agent-1",
        -- wallpaper
        "swww-daemon",
        -- audio
        "pipewire",
        "wireplumber",
        -- notifications
        "mako",
        -- DE things
        "waybar",
        "hypridle",
        -- other
        "etesync-dav",
        "protonmail-bridge"
    },
    service_bins = {
        ["/usr/lib/polkit-kde-authentication-agent-1"] = "polkit-kde-authentication-agent-1"
    }
}
return M
